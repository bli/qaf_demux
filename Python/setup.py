from setuptools import setup, find_packages
# If you have .pyx things to cythonize
#from Cython.Build import cythonize

__version__ = "0.2"


setup(
    name="qaf_demux",
    version=__version__,
    description=".",
    author="Blaise Li",
    author_email="blaise.li__github@nsup.org",
    license="MIT",
    packages=find_packages(),
    scripts=["bin/qaf-demux"],
    install_requires=["cytoolz"])#,
# If you have .pyx things to cythonize
    #ext_modules = cythonize("qaf_demux/libqafdemux.pyx"),
    #zip_safe=False)
