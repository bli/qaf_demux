Qaf demux
=========

This program demultiplexes fastq data having "in-line" barcodes based on a
given list of barcodes and a barcode start position. It tries to assign each
fastq record to the most likely barcode, taking into account the sequence
qualities (interpreted as being sanger-encoded).

A similar (but more sophisticated) approach is available here:
<https://github.com/grenaud/deML>

The program uses the QafDemux module that provides the following functions:

```@meta
# docs
# dist(bc1::AbstractString, bc2::AbstractString)

# qualnum2prob(qual::UInt8)
```

```@autodocs
Modules = [QafDemux]
```

### Just testing doctests

```@meta
DocTestSetup = quote
    using QafDemux
end
```

```jldoctest
julia> qualnum2prob(40)
0.0003
```

```@meta
DocTestSetup = nothing
```
