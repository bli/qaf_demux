push!(LOAD_PATH, abspath(joinpath(@__DIR__, "../src/")))
# push!(LOAD_PATH, "../src/")
import Pkg
Pkg.add("Documenter")
using Documenter, QafDemux

makedocs(sitename="Qaf demux",
    doctest = true,
    format = Documenter.HTML(
        prettyurls = get(ENV, "CI", nothing) == "true"
    )
)
