#!/bin/bash

wrapper=$(readlink -f "${BASH_SOURCE[0]}")
echo ${wrapper}
PACKAGEDIR=$( cd "$( dirname "${wrapper}" )/.." && pwd )
wrapper_name=$(basename "${wrapper}")

julia_script="${PACKAGEDIR}/bin/${wrapper_name%.sh}.jl"

/usr/bin/env julia --project=${PACKAGEDIR} ${julia_script} "$@"
