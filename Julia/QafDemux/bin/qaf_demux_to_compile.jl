#!/usr/bin/env julia

push!(LOAD_PATH, abspath(joinpath(@__DIR__, "../src/")))
import QafDemux
const qd = QafDemux

Base.@ccallable function julia_main(ARGS::Vector{String})::Cint
    return qd.main()
end
