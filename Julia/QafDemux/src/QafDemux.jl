__precompile__()
module QafDemux

doc = """
This program demultiplexes fastq data having "in-line" barcodes based on a
given list of barcodes and a barcode start position. It tries to assign each
fastq record to the most likely barcode, taking into account the sequence
qualities (interpreted as being sanger-encoded).

A similar (but more sophisticated) approach is available here:
<https://github.com/grenaud/deML>
"""


import IterTools
const it = IterTools
using ArgParse
import CodecZlib
const gz = CodecZlib
import TranscodingStreams
const ts = TranscodingStreams
# To install FASTX in the REPL
#]registry add https://github.com/BioJulia/BioJuliaRegistry.git
#]add FASTX
import FASTX
const fq = FASTX.FASTQ


export qualnum2prob, dist, make_record_reader, make_record_dispatcher, make_record_writers, make_seq_qual_extractor, parse_commandline, main


"""
    qualnum2prob(qual::UInt8)

Compute the probability of error associated with the fastq quality value `qual`.

```jldoctest
julia> qualnum2prob(40)
0.0002

```
"""
qualnum2prob(qual::UInt8) = 10 ^ -(qual / 10)
qualnum2prob(qual::Int64) = qualnum2prob(convert(UInt8, qual))


"""
    qualchar2prob(letter::Char)

Compute the probability of error associated with the fastq Phred+33 quality `letter`.

```jldoctest
julia> qualchar2prob('I')
0.0001

```
"""
qualchar2prob(letter::Char) = 10 ^ ((33 - Int(letter)) / 10)


"""
    dist(bc1::AbstractString, bc2::AbstractString)

Compute the distance between barcodes `bc1` and `bc2`.

Barcodes are assumed to be the same length.

```jldoctest
julia> dist("TTAGGC", "TTAGGC")
0

julia> dist("TTAGGC", "CAGATC")
5

julia> dist("TTAGGC", "AAAACT")
5

julia> dist("TTAGGC", "GCCAAT")
6

```
"""
function dist(bc1::String, bc2::String)::UInt8
    nb_diffs = 0
    for pos in 1:length(bc1)
        if bc1[pos] != bc2[pos]
            nb_diffs += 1
        end
    end
    nb_diffs
end


struct SeqProb
    nucl::Char
    err_prob::Float64
end


# function bc_prob_profile(seq, quals)
#     prob_profile = Vector{SeqProb}(undef, length(quals))
#     for qual in quals
#         push!(prob_profile, SeqProb(nucl, qualnum2prob(qual)))
#     end
#     return prob_profile
# end


"""
    make_bc_prob_profile(seq, quals)

Create an array of SeqProb items associating a nucleotide and the probability
of it being incorrectly read given its fastq quality.
"""
function make_bc_prob_profile(seq::String, quals::Vector{UInt8})::Vector{SeqProb}
    return [SeqProb(nucl, err_prob) for (nucl, err_prob) in zip(seq, qualnum2prob.(quals))]
end


"""
    bc_like(bc_prob_profile::Vector{SeqProb}, barcode::String)

Compute the likelihood of barcode `barcode`
given the error probability profile `bc_prob_profile`.
"""
function bc_like(bc_prob_profile::Vector{SeqProb}, barcode::String)::Float64
    # TODO: handle N correctly
    bc_prob = 1.0
    for (seqprob, nucl) in zip(bc_prob_profile, barcode)
        if seqprob.nucl == nucl
            # This position's contribution to the
            # probability for the barcode is the
            # probability that this letter is correct
            bc_prob *= (1.0 - seqprob.err_prob)
        else
            # This position's contribution to the
            # probability for the barcode is the
            # probability that this letter is not correct
            # and that the real one is the one among the
            # three other that actually is bc_letter
            bc_prob *= (seqprob.err_prob / 3.0)
        end
    end
    return bc_prob
    # TODO: test this:
    # prob(
    #     ((seqprob, nucl)->seqprob.nucl == nucl?(1 - seqprob.err_prob):(seqprob.err_prob / 3)),
    #     zip(bc_prob_profile, barcode))
end


function find_best_barcode_func(prob_profile::Vector{SeqProb}, barcodes::Vector{String})::Tuple{String,Float64}
    bc_likes_dict = Dict{String,Float64}(
        (barcode => prob) for (barcode, prob) in zip(barcodes, map(bc -> bc_like(prob_profile, bc), barcodes)))
    (best_prob, best_barcode) = findmax(bc_likes_dict)
    return (best_barcode, best_prob)
end


function find_best_barcode_proc(prob_profile::Vector{SeqProb}, barcodes::Vector{String})::Tuple{String,Float64}
    best_barcode = barcodes[1]
    best_prob = bc_like(prob_profile, best_barcode)
    for barcode in barcodes[2:end]
        this_prob = bc_like(prob_profile, barcode)
        if this_prob > best_prob
            best_barcode = barcode
            best_prob = this_prob
        end
    end
    return (best_barcode, best_prob)
end


# https://bicycle1885.github.io/TranscodingStreams.jl/latest/examples/#Use-a-noop-codec-1
function chosetranscoder(filepath::String)
    if endswith(filepath, ".gz")
        return gz.GzipDecompressorStream
    else
        return ts.NoopStream
    end
end


function make_seq_qual_extractor(bc_range::UnitRange{Int})
    function seq_qual_extractor(record::fq.Record)::Tuple{String,Vector{UInt8}}
        subseq = fq.sequence(String, record, bc_range)
        quals = fq.quality(record, :illumina18, bc_range)
        return (subseq, quals)
    end
end


function make_seq_qual_extractor(bc_start::Int, bc_len::Int)
    @assert bc_start < 0 "*bc_start* should be negative."
    function seq_qual_extractor(record::fq.Record)::Tuple{String,Vector{UInt8}}
        seq = fq.sequence(String, record)
        seq_len = length(seq)
        real_start = seq_len + bc_start + 1
        bc_range = real_start:(real_start + (bc_len - 1))
        quals = fq.quality(record, :illumina18, bc_range)
        return (seq[bc_range], quals)
    end
end


# TODO: make this able to read from stdin
function make_record_reader(fq_filename::String, barcodes::Vector{String}, seq_qual_extractor, func_style=true)
    transcoder = chosetranscoder(fq_filename)
    function record_reader(out_c::Channel)
        # https://biojulia.net/FASTX.jl/latest/manual/fastq/
        # open(ts.NoopStream, fq_filename) do stream
        open(transcoder, fq_filename) do fq_filestream
            reader = fq.Reader(fq_filestream)
            record = fq.Record()
            while !eof(reader)
                read!(reader, record)
                # Now we have a new record
                # Extract the portion of the sequence and qualities
                # located at barcode positions.
                # subseq = convert(String, fq.sequence(record, bc_range))
                # subseq = fq.sequence(String, record, bc_range)
                # quals = fq.quality(record, :illumina18, bc_range)
                (subseq, quals) = seq_qual_extractor(record)
                # Array of SeqProb items,
                # each having a nucl and a err_prob attributes.
                prob_profile = make_bc_prob_profile(subseq, quals)
                # TODO: test which is faster
                if func_style
                    (best_barcode, best_prob) = find_best_barcode_func(prob_profile, barcodes)
                else
                    # This one seems slightly faster, but the results are different.
                    # They make different choices in case of prob equality.
                    # If min_dist and max_prob are well chosen,
                    # this should only affect Undetermined.
                    (best_barcode, best_prob) = find_best_barcode_proc(prob_profile, barcodes)
                end
                best_diff = dist(best_barcode, subseq)
                # println("best_barcode: $best_barcode (p: $best_prob, d: $best_diff)")
                if fq.hasdescription(record)
                    new_description = "$(fq.description(record)) $best_barcode:$best_prob:$best_diff"
                else
                    new_description = "$best_barcode:$best_prob:$best_diff"
                end
                put!(
                    out_c,
                    (best_barcode, best_prob, best_diff, fq.Record(
                        fq.identifier(record),
                        new_description,
                        fq.sequence(record),
                        fq.quality(record))))
            end
        end
        # close(reader)
    end
end


function make_record_writers(callback, outfile_paths_dict::Dict{String,String})
    record_writers = Dict(
        barcode => fq.Writer(gz.GzipCompressorStream(open(fq_filename, "w")))
        for (barcode, fq_filename) in outfile_paths_dict)
    try
        callback(record_writers)
    finally
        for (barcode, fq_writer) in record_writers
            # Write at least nothing, so that empty gzip is valid
            # when no records were written to a file.
            write(fq_writer.output, "")
            close(fq_writer)
        end
        @info "Fastq files have been written:" outfile_paths_dict
    end
end


# function make_record_dispatcher(record_writers, max_diff, min_prob)
function make_record_dispatcher(record_writers, max_diff::UInt8, min_prob::Float64)
    function record_dispatcher(record_info::Tuple{String,Float64,UInt8,fq.Record})
        (best_barcode, best_prob, best_diff, record) = record_info
        if best_diff <= max_diff && best_prob >= min_prob
            write(record_writers[best_barcode], record)
        else
            # TODO: Record info about reason for failure
            # to assign to a barcode.
            write(record_writers["Undetermined"], record)
        end
    end
end


function parse_commandline(cmdline=nothing)
    settings = ArgParseSettings(description = doc)
    @add_arg_table settings begin
        "--input_fastq", "-i"
            help = "Fastq file to demultiplex."
            action = :store_arg
        "--output_dir", "-o"
            help = "Directory in which to put the demultiplexed fastq files."
            action = :store_arg
        "--barcodes", "-b"
            help = "Barcodes to which the reads should be attributed."
            nargs = '+'
            action = :store_arg
            arg_type = String
        "--barcode_start", "-s"
            help = "Position at which the barcode starts (1-based). Use a negative value to indicate a position counted from the read end."
            action = :store_arg
            arg_type = Int
            required = true
        "--max_diff", "-m"
            help = "Only assign a record to one of the barcodes if it has no more than *max_diff* differences in its barcode portion."
            action = :store_arg
            arg_type = Int
            default = 3
        "--min_prob", "-p"
            help = "Only assign a record to one of the barcodes if that barcode has at least *min_prob* likelihood."
            action = :store_arg
            arg_type = Float64
            default = 0.2
    end
    if cmdline == nothing
        return parse_args(ARGS, settings)
    else
        return parse_args(cmdline, settings)
    end
end


function main(cmdline=nothing)
    args = parse_commandline(cmdline)
    @debug "parsed arguments:" args

    fq_filename = args["input_fastq"]
    barcodes = args["barcodes"]
    out_dir = args["output_dir"]
    mkpath(out_dir)
    outfile_paths = Dict(
        barcode => joinpath(out_dir, "$barcode.fastq.gz")
        for barcode in barcodes)
    outfile_paths["Undetermined"] = joinpath(out_dir, "Undetermined.fastq.gz")
    bc_len = length(barcodes[1])
    for barcode in barcodes
        @assert length(barcode) == bc_len "All barcodes should have the same length."
    end
    bc_start = args["barcode_start"]
    # "standalone" ranges "from end" are not possible
    if bc_start < 0
        # bc_range has to be computed for each read based on its length
        seq_qual_extractor = make_seq_qual_extractor(bc_start, bc_len)
    else
        bc_range = bc_start:bc_start+(bc_len-1)
        seq_qual_extractor = make_seq_qual_extractor(bc_range)
    end
    max_diff = UInt8(args["max_diff"])
    min_prob = args["min_prob"]
    bc_diffs = [dist(bc1, bc2) for (bc1, bc2) in it.subsets(barcodes, 2)]
    min_bc_diff = min(bc_diffs...)
    # @info "Smallest distance between barcodes:" Int64(min_bc_diff)
    @info "Smallest distance between barcodes: $min_bc_diff"
    if max_diff >= min_bc_diff
        @warn "Some barcodes are too close (min_diff: $min_bc_diff) for reliable assignment with max_diff=$max_diff."
    end
    # Dict where keys are barcodes and values are fastq record writers
    make_record_writers(outfile_paths) do record_writers
        dispatch_record = make_record_dispatcher(record_writers, max_diff, min_prob)
        for record_info in Channel(make_record_reader(fq_filename, barcodes, seq_qual_extractor, true))
            dispatch_record(record_info)
        end
    end
    return 0
end

Base.@ccallable function julia_main(ARGS::Vector{String})::Cint
    return main()
end

end # module
