# Package

version       = "0.1.2.1"
author        = "Blaise Li"
description   = "This program tries to attribute each input fastq record to the most likely barcode among a predefined set, given the expected barcode starting position within the reads."
license       = "MIT"
srcDir        = "src"
binDir        = "bin"
bin           = @["qaf_demux"]
# Binary packages should not install .nim files
skipExt       = @["nim"]

# Dependencies

requires "nim >= 1.4.2"
# 0.7.0 seems still compatible with the toolchain provided by
# `singularity shell docker://nimlang/nim:1.4.2-regular`
requires "docopt == 0.7.0"
# breaks at the next commit
#requires "docopt#c399e0d9ed55f2122a82de43150361e881f69e3c"
requires "zip#head"
# requires "memo#head"
