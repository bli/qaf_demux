#import nimprof
# To use parallel
#{.experimental.}
#import threadpool
import docopt
# To get list comprehension
from sugar import collect
import tables as tbl
import zip/gzipfiles
from math import pow, sum
from strutils import ffScientific, formatFloat, intToStr, join, parseInt, parseFloat
from sequtils import zip
from streams import newFileStream, Stream, atEnd, readLine
# from memo import memoized

# https://github.com/docopt/docopt.nim
const doc = """
This program demultiplexes fastq data having "in-line" barcodes based on a
given list of barcodes and a barcode start position. It tries to assign each
fastq record to the most likely barcode, taking into account the sequence
qualities (interpreted as being sanger-encoded).

A similar (but more sophisticated) approach is available here:
<https://github.com/grenaud/deML>

Usage:
  qaf-demux [-i <input_fastq> | --input_fastq <input_fastq>] [-g | --gzip_input] (-o <output_dir> | --output_dir <output_dir>) -b <barcode1>... -s <barcode_start> [-m <max_diff>] [-p <min_prob>]

-h --help                                 Show this help message and exit.
-i --input_fastq <input_fastq>            Fastq file to demultiplex. Default is to read from stdin.
-g --gzip_input                           Indicate that input is gzipped.
-o --output_dir <output_dir>              Directory in which to put demultiplexed fastq files.
-b --barcode <barcode> [-b <barcode>...]  Barcodes to which the reads should be attributed.
-s --barcode_start <barcode_start>        Position at which the barcode starts (1-based).
-m --max_diff <max_diff>                  Only assign a record to one of the barcodes if it has no more than *max_diff* differences in its barcode portion. [default: 3]
-p --min_prob <min_prob>                  Only assign a record to one of the barcodes if that barcode has at least *min_prob* likelihood. [default: 0.2]
"""


type
  # read name, nucleotides and qualities
  Fastq = array[0..2, string]
  # nucleotide and probability of it having been mis-read
  NucProb = tuple
    nucl: char
    prob: float
  # mySlice = Slice[BackwardsIndex] | Slice[int]
  ProbTable = array[0..41, float]


# proc `==` (ns, ms: Nucleotides): bool =
#   string(ns) == string(ms)
# https://nim-by-example.github.io/types/distinct/
#proc `==` (ns, ms: Nucleotides): bool {.borrow.}


#proc makeFastq(name, nucls, quals: string): Fastq {.inline.} =
#  #result = (name: name, nucls: nucls.Nucleotides, quals: quals.Qualities)
#  result = (name: name, nucls: nucls, quals: quals)

# We define the "stringify" operator `$` for Fastq, that is,
# how to turn a Fastq record into 4 lines of text in fastq format
# proc `$` (record: Fastq): string =
#   result = join([
#     record.name,
#     record.nucls,
#     "+",
#     record.quals], "\n") & "\n"
proc `$` (record: Fastq): string {.inline.} =
  result = join([
    record[0],
    record[1],
    "+",
    record[2]], "\n") & "\n"


iterator fastqParser(stream: Stream): Fastq =
  var
    nameLine: string
    nucLine: string
    quaLine: string
  while not stream.atEnd():
    nameLine = stream.readLine()
    # echo("nameLine:", nameLine)
    #TODO: Why is there an extra empty Fastq when reading gzipped input?
    # https://github.com/nim-lang/zip/issues/26
    # Fixed here:
    # https://github.com/nim-lang/zip/commit/49620e45de391d1deeb6ed1195f672107855f124
    # if nameLine == "":
    #   break
    #doAssert(not stream.atEnd(), "stream ended after nameLine: " & nameLine)
    nucLine = stream.readLine()
    # echo("nucLine:", nucLine)
    #doAssert(not stream.atEnd(), "stream ended after nucLine: " & nucLine)
    discard stream.readLine()
    #doAssert(not stream.atEnd(), "stream ended after quaLine: " & quaLine)
    quaLine = stream.readLine()
    # echo("quaLine:", quaLine)
    #yield (nameLine, nucLine, quaLine)
    yield [nameLine, nucLine, quaLine]


#proc makeBarcodeGetter(bcStart, bcLen: int): proc {.inline.} =
#  result = proc(sequence: string): string =
#    sequence[(bcStart-1)..<(bcStart + bcLen - 1)]



proc dist(bc1, bc2: string): int {.inline.} =
  #mixin `|`, `<-`
  #sum(lc[int(pair.a != pair.b) | (pair <- zip(bc1, bc2))])
  result = 0
  for pair in zip(bc1, bc2):
    #result = result + int(pair.a != pair.b)
    if pair[0] != pair[1]:
      inc(result)


# Might be worth memoizing since there are only a few possible results
# proc qualToProb(letter: char): float {.memoized.} = pow(10, ((33 - ord(letter)) / 10))
#proc qualToProb(letter: char): float = pow(10, ((33 - ord(letter)) / 10))
# This works well:
# let qualToProb = collect(tbl.newTable()):
#   for letter in items("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"):
#     {letter:  pow(10, ((33 - ord(letter)) / 10))}
#
# This works nicely, but {.compileTime.} doesn't seem to help
# proc initQualToProbs(): ProbTable {.compileTime.} =
#   for i in 0..41:
#     result[i] = pow(10, (-i) / 10)
#
# const qualToProb: ProbTable = initQualToProbs()

# This works too, not sure about the effect of {.compileTime.}
const qualToProb: ProbTable = block:
  var qualToProb {.compileTime.}: ProbTable
  for i in 0..41:
    qualToProb[i] = pow(10, (-i) / 10)
  qualToProb


# Return a sequence of (char, float)
# representing (nucleotide, probability of being mis-read) pairs
proc nucProbs(sequence, qualities: string): seq[NucProb] =
  # let N = min(sequence.len, qualities.len)
  # result = newSeq[NucProb](N)
  result = newSeq[NucProb](min(sequence.len, qualities.len))
  for i in 0..result.high:
    result[i] = (nucl: sequence[i], prob: qualToProb[ord(qualities[i]) - 33])
    # result[i] = (nucl: sequence[i], prob: qualToProb[qualities[i]])
    # result[i] = (nucl: sequence[i], prob: qualToProb(qualities[i]))


proc demultiplex(inputFqs: Stream, outDir: string, barcodes: seq[string], bcStart, maxDiff: int, minProb: float) =
  var
    outfiles = tbl.newTable[string, Stream]()
    allowed: string
    # distances between barcodes
    bcLen: int = barcodes[0].len
  #   bcSlice: mySlice
  # if bcStart < 0:
  #   bcSlice = ^(bcLen - bcStart - 1)..^(-bcStart)
  # else:
  #   bcSlice = (bcStart-1)..<(bcStart + bcLen - 1)
  # let bcSlice =
  #   if bcStart < 0:
  #     ^(bcLen - bcStart - 1)..^(-bcStart)
  #   else:
  #     (bcStart-1)..<(bcStart + bcLen - 1)
  # Prepare table of output files
  # echo("bcStart:", bcStart)
  # echo("bcLen:", bcLen)
  for allowed in barcodes:
    doAssert(allowed.len == bcLen)
    let outfile = newGZFileStream(outDir & "/" & allowed & ".fastq.gz", fmWrite)
    outfiles[allowed] = outfile
  outfiles["Undetermined"] = newGZFileStream(outDir & "/Undetermined.fastq.gz", fmWrite)
  #var bcGetter = makeBarcodeGetter(bcStart, bcLen)
  proc matchToAllowed(fastq: Fastq): (string, string) =
    var
      bcSeq: string
      bcQuals: string
    #let bcSeq = bcGetter(fastq.nucls)
    #let bcQuals = bcGetter(fastq.quals)
    # let bcSeq = fastq.nucls[(bcStart-1)..<(bcStart + bcLen - 1)]
    # let bcQuals = fastq.quals[(bcStart-1)..<(bcStart + bcLen - 1)]
    # echo("fastq[1]:", fastq[1])
    # echo("fastq[1].len:", fastq[1].len)
    # echo("bcStart-1:", bcStart-1)
    # echo("bcStart + bcLen - 1:", bcStart + bcLen - 1)
    # let bcSeq = fastq[1][(bcStart-1)..<(bcStart + bcLen - 1)]
    # let bcQuals = fastq[2][(bcStart-1)..<(bcStart + bcLen - 1)]
    if bcStart < 0:
      bcSeq = fastq[1][^(-bcStart)..^(-((bcLen-1)+bcStart))]
      bcQuals = fastq[2][^(-bcStart)..^(-((bcLen-1)+bcStart))]
    else:
      bcSeq = fastq[1][(bcStart-1)..<(bcStart + bcLen - 1)]
      bcQuals = fastq[2][(bcStart-1)..<(bcStart + bcLen - 1)]
    # let bcSeq = fastq[1][bcSlice]
    # let bcQuals = fastq[2][bcSlice]
    let bcProbs = nucProbs(bcSeq, bcQuals)
    #let bcProbs = nucProbs(bcSeq, fastq[2][(bcStart-1)..<(bcStart + bcLen - 1)])
    proc likelihood(barcode: string): float =
      var letter: char
      var errProb: float
      result = 1
      for i, np in bcProbs.pairs():
        # nucleotide, probability of being mis-read
        (letter, errProb) = np
        if letter == barcode[i]:
          result = result * (1 - errProb)
        else:
          # If the read nucleotide differs from the barcode nucleotide
          # it could actually still be the correct one
          # if there was an error when reading it.
          # We assume that the 3 nucleotides other than the one having been read
          # are equiprobable (this might not be true).
          result = result * (errProb / 3)
      # parallel:
      #   for i in 0..<N:
      #     (letter, errProb) = bcProbs[i]  # Error: cannot prove: i <= len(:envP.bcProbs2) + -1 (bounds check)
      #     if letter == barcode[i]:
      #       result = result * (1 - errProb)
      #     else:
      #       result = result * (errProb / 3)
    # mixin `|`, `<-`
    # let likelihoods = lc[likelihood(allowed) | (allowed <- barcodes), float]
    let likelihoods = collect(newSeq):
      for allowed in barcodes:
        likelihood(allowed)
    var maxLike = likelihoods[0]
    var bestIdx = 0
    # https://stackoverflow.com/a/48123056/1878788
    for idx, like in pairs(likelihoods):
      if like > maxLike:
        maxLike = like
        bestIdx = idx
    let candidate = barcodes[bestIdx]
    let theDist = dist(bcSeq, candidate)
    let annot = join([
      candidate,
      formatFloat(maxLike, ffScientific),
      theDist.intToStr], ":")
    if theDist <= maxDiff and maxLike >= minProb:
      result = (candidate, annot)
    else:
      result = ("Undetermined", annot)
    # if theDist > maxDiff or maxLike < minProb:
    #   result = ("Undetermined", annot)
    # else:
    #   result = (candidate, annot)
    # result = findClosest(bcSeq)
  for fastq in fastqParser(inputFqs):
    let (closest, annot) = matchToAllowed(fastq)
    # outfiles[closest].write($(
    #   name: fastq.name & " " & annot,
    #   nucls: fastq.nucls,
    #   quals: fastq.quals))
    outfiles[closest].write($[
      fastq[0] & " " & annot,
      fastq[1],
      fastq[2]])
    # outfiles[closest].write($(
    #   name: fastq.name & " " & annot,
    #   nucls: fastq.nucls,
    #   quals: fastq.quals))
  for allowed in barcodes:
    outfiles[allowed].close()
  outfiles["Undetermined"].close()

when isMainModule:
  let args = docopt(doc)
  var
    inFastqFilename: Value
    openGZ: bool = false
    outDir: string
    barcodes: seq[string]
    bcStart, maxDiff: int
    minProb: float
  for opt, val in args.pairs():
    case opt
    of "-i", "--input_fastq":
      inFastqFilename = val
    of "-g", "--gzip_input":
      openGZ = val
    of "-o", "--output_dir":
      outDir = $args[opt]
    of "-b", "--barcode":
      barcodes = @(val)
    of "-s", "--barcode_start":
      bcStart = parseInt($val)
    of "-m", "--max_diff":
      maxDiff = parseInt($val)
    of "-p", "--min_prob":
      minProb = parseFloat($val)
    else:
      echo "Unknown option" & opt
      quit(QuitFailure)
  #for barcode in barcodes:
  #  outfiles[barcode].write(barcode & "\n")
  let inputStream =
    if not inFastqFilename:
      if openGZ:
      #   toClosure(parseFastqs(newGZFileStream("stdin")))
        newGZFileStream("/dev/stdin")
      else:
      #   toClosure(parseFastqs(newFileStream(stdin)))
        newFileStream(stdin)
      #toClosure(parseFastqs(newFileStream(stdin)))
      # newFileStream(stdin))
    else:
      if openGZ:
      #   toClosure(parseFastqs(newGZFileStream($inFastqFilename)))
        newGZFileStream($inFastqFilename)
      else:
        let fh = open($inFastqFilename)
        newFileStream(fh)
      #   toClosure(parseFastqs(newFileStream(fh)))
      #let fh = open($inFastqFilename)
      #toClosure(parseFastqs(newFileStream(fh)))
      #fastqParser(newFileStream(fh))
  demultiplex(inputStream, outdir, barcodes, bcStart, maxDiff, minProb)
