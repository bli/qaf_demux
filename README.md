Quality-aware fastq demultiplexer (qaf-demux)
=============================================

This program demultiplexes fastq data having "in-line" barcodes, based on
a given list of barcodes and a barcode start position. It tries to assign
each fastq record to the most likely barcode, taking into account the
sequence qualities (interpreted as being sanger-encoded).

A similar (but more sophisticated) approach is available here:
<https://github.com/grenaud/deML>

There are several versions of this program, with slightly
different calling syntaxes:

* A Nim version, residing in `./Nim`, and called using `qaf_demux` (with
  an underscore).

* A Python version, residing in `./Python/bin`, and called using
  `qaf-demux` (with a dash).

* A Julia version, residing in `./Julia/QafDemux/bin`, called using a
  wrapper shell script `qaf_demux.sh`.

The Nim version should be built beforehand using the `nimble` package
manager (directly or via the provided `Makefile`).
It should be faster than the Python version.
A Galaxy xml wrapper for this version is available here:
<https://toolshed.pasteur.fr/view/bli/qaf_demux/1917f4badddf>

The Julia version should be faster than the Python version on large
enough files, however, due to difficulties with keeping up-to-date with
latest Julia versions, it is not maintained any more.


Installation
============


Nim version
-----------

If you haven't already done it, you first need to [install
Nim](https://nim-lang.org/install_unix.html) (you might consider using
[choosenim](https://github.com/dom96/choosenim#choosenim) to do the
installation).

Then you can install the program locally (in your `~/.nimble/bin` directory) using `nimble`:

```
cd Nim
nimble install
```

If you need to install it somewhere else, either run the above and then
manually copy the resulting binary where you need, or use the provided
`./Nim/Makefile`, setting the `PREFIX` environment variable:

```
cd Nim
make
sudo PREFIX=/opt make install
```

### Singularity container

A recipe to build a singularity container is also provided:

* `Nim/singularity/qaf_demux_nim.def`: based on a Linux Alpine container

It can be automatically built using the provided `Makefile`:

```
cd Nim/singularity
sudo make
```

Building singularity images require administrative rights, and of course, singularity.
This building method has been tested with singularity versions 3.4 and 3.7.


Python version
--------------

To be able to run this version, you need Python 3.6 or above.

Then, you can install it either using the `./Python/setup.py` script:

```
cd Python
python3 setup.py install
```

or using `pip3`:

```
cd Python
pip3 install .
```

If you get an error message like `ImportError: cannot import name 'main'`, you
may try the following instead:

```
cd Python
python3 -m pip install .
```

Note: The `Python/install.sh` script is what I personally use to install
the program.
It uses the `-e` option of pip that avoids having to reinstall after
editing the source (which, by the way, is in `./Python/bin/qaf-demux`).
You can use this if you want to modify the script to better suit your
needs.


Julia version
-------------


### Compiled executable


**The build procedure does not seem to work any more with julia 1.3 and above.**

A compiled executable can be built, that should be faster to start than
the wrapped Julia script.

To build it, proceed as follows:

```
cd Julia/QafDemux
julia --project=. --eval 'import Pkg; \
    Pkg.activate("."); Pkg.instantiate(); Pkg.build();'
```

This obviously requires a Julia installation.

This build method has been tested with Julia 1.2, and [may not work with a
locally-compiled version of Julia](https://github.com/NHDaly/ApplicationBuilder.jl/issues/62#issuecomment-503721859).

This may also fail if some tools such as `llvm` are not present, or
present but with an incompatible version (unfortunately, I don't know
what versions are compatible).

The resulting executable, along with the necessary shared libraries, will
be found in `Julia/QafDemux/deps/builddir`.


### Singularity containers

Recipes to build singularity containers are also provided:

* `Julia/QafDemux/singularity/qaf_demux_juila.def`: based on a debian stretch container (not
  yet working as of 24/09/2019, because the needed docker image is not
  available on docker hub)
* `Julia/QafDemux/singularity/qaf_demux_juila_local.def`: uses a locally installed debian
  stretch docker image (see
  https://github.com/sylabs/singularity/issues/1537#issuecomment-388642244)
* `Julia/QafDemux/singularity/qaf_demux_juila_buster.def`: based on a debian buster container

The first two can be automatically built using the provided `Makefile`:

```
cd Julia/QafDemux/singularity
sudo make qaf_demux
```

or

```
cd Julia/QafDemux/singularity
sudo make qaf_demux_local
```

Building singularity images require administrative rights, and of course, singularity.
This building method has been tested with singularity 3.4.

The resulting containers (`Julia/QafDemux/singularity/qaf_demux` or
`Julia/QafDemux/singularity/qaf_demux`) can be directly executed.

The `qaf_demux_local` container was minimally tested on a centos cluster having
[environment modules](https://modules.readthedocs.io/en/stable/index.html):

```
$ module load singularity/3.4.0
$ ./qaf_demux_local --help
usage: <PROGRAM> [-i INPUT_FASTQ] [-o OUTPUT_DIR]
                 [-b BARCODES [BARCODES...]] -s BARCODE_START
                 [-m MAX_DIFF] [-p MIN_PROB] [-h]

This program demultiplexes fastq data having "in-line" barcodes based
on a given list of barcodes and a barcode start position. It tries to
assign each fastq record to the most likely barcode, taking into
account the sequence qualities (interpreted as being sanger-encoded).
A similar (but more sophisticated) approach is available here:
<https://github.com/grenaud/deML>

optional arguments:
  -i, --input_fastq INPUT_FASTQ
                        Fastq file to demultiplex.
  -o, --output_dir OUTPUT_DIR
                        Directory in which to put the demultiplexed
                        fastq files.
  -b, --barcodes BARCODES [BARCODES...]
                        Barcodes to which the reads should be
                        attributed.
  -s, --barcode_start BARCODE_START
                        Position at which the barcode starts
                        (1-based). Use a negative value to indicate a
                        position counted from the read end. (type:
                        Int64)
  -m, --max_diff MAX_DIFF
                        Only assign a record to one of the barcodes if
                        it has no more than *max_diff* differences in
                        its barcode portion. (type: Int64, default: 3)
  -p, --min_prob MIN_PROB
                        Only assign a record to one of the barcodes if
                        that barcode has at least *min_prob*
                        likelihood. (type: Float64, default: 0.2)
  -h, --help            show this help message and exit

```

However, it currently does not work when tested "for real" (i.e. when it comes
to process data).



Usage
=====

The `-h` (or `--help`) option gives you usage information.


Python version
--------------

The installed Python binary is named `qaf-demux`.

```
qaf-demux -h
```

The options are the following:

* `-i` indicates the input file (it must be either in fastq or gzipped
  fastq format).

* `-b` sets the list of barcodes (space-separated).

* `-s` indicates the (1-based) starting position of the barcode.

* `-m` sets the maximum allowed number of mismatches with respect to the
  most likely barcode found. If the barcode region of a read exceeds this
number of mismatches with respect to the most likely barcode, it will be
considered undetermined.

* `-p` sets the minimum likelihood allowed for the most likely barcode to
  be actually chosen. If the likelihood is lower than this value, the
read will be considered undetermined.

* `-o` sets the directory where the demultiplexed data will be written.

The output directory will contain one `.fastq.gz` file for each barcode,
plus one for the undetermined reads. The file names start with the
corresponding barcode, or "Undetermined".

Example usage:

```
qaf-demux -i test_data/iCLIP_CSR1_090118_1M.fastq.gz \
    -o demultiplexed \
    -b TTAGGC ACAGTG GCCAAT CAGATC \
    -s 6 -m 2
```

The output for the above example should consist in 5 files:

```
demultiplexed/ACAGTG.fastq.gz
demultiplexed/CAGATC.fastq.gz
demultiplexed/GCCAAT.fastq.gz
demultiplexed/TTAGGC.fastq.gz
demultiplexed/Undetermined.fastq.gz
```


Nim version
-----------

The installed Nim binary is named `qaf_demux`.

```
qaf_demux -h
```

If you use the singularity container:

```
./Nim/singularity/qaf_demux -h
```


The options are the following:

* `-i` indicates the input file (it must be either in fastq or gzipped
  fastq format, also use `-g` in this case). Alternatively, the program
can read fastq (or gzipped fastq) data on its standard input.

* `-g` indicates that the input is gzipped. The program might not
  complain if you omit this option, but meaningful results are not
guaranteed.

* `-b` indicates a possible barcode (one such option for each barcode).

* `-s` indicates the (1-based) starting position of the barcode.

* `-m` sets the maximum allowed number of mismatches with respect to the
  most likely barcode found. If the barcode region of a read exceeds this
number of mismatches with respect to the most likely barcode, it will be
considered undetermined.

* `-p` sets the minimum likelihood allowed for the most likely barcode to
  be actually chosen. If the likelihood is lower than this value, the
read will be considered undetermined.

* `-o` sets the directory where the demultiplexed data will be written.

The output directory will contain one `.fastq.gz` file for each barcode,
plus one for the undetermined reads. The file names start with the
corresponding barcode, or "Undetermined".

Example usage:

```
qaf_demux -i test_data/iCLIP_CSR1_090118_1M.fastq.gz \
    -g \
    -o demultiplexed \
    -b TTAGGC -b ACAGTG -b GCCAAT -b CAGATC \
    -s 6 -m 2
```

The output for the above example should consist in 5 files:

```
demultiplexed/ACAGTG.fastq.gz
demultiplexed/CAGATC.fastq.gz
demultiplexed/GCCAAT.fastq.gz
demultiplexed/TTAGGC.fastq.gz
demultiplexed/Undetermined.fastq.gz
```

### Main differences with the Python version

You can pass the data to the program's standard input instead of using the `-i` option.

The output directory must exist before you run the program.

Use as many `-b` options as there are barcodes.

Don't forget the `-g` option if the data is gzipped.


Julia version
-------------


The options are the same as for the Python version.

If you use the wrapper script:

```
./Julia/QafDemux/bin/qaf_demux.sh -h
```

If you use the compiled executable:
```
./Julia/QafDemux/deps/builddir/qaf_demux -h
```

If you use the singularity container:
```
./Julia/QafDemux/qaf_demux -h
```


Barcode start position specified relative to the read end
---------------------------------------------------------

For all versions, the value given to option `-s` can be either positive
or negative.

If it is a positive number, this straightforwardly indicates the
(1-based) staring position for the barcode portion in the reads. For
instance, `-s 1` indicates that the barcode is expected to start at the
first position of the read whereas `-s 3`indicates that the first two
positions are not part of the barcode, which only starts at the third
one:

            123
    read    RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    barcode   BBBBBBB


If it is a negative number, this indicates a position counted backwards
from the end of the read (`-1` being the last position). For instance,
`-s -9` indicates that the barcode starts at the 9nth position of the
read counted from the end (and the 2nd position of the barcode is
expected to correspond to the 8nth position from the end, etc.):

                                       987654321
    read    RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    barcode                            BBBBBBB


(There should be enough room at the end of the read for the barcode to
fit, otherwise, expect issues or incorrect read assignment when running
the program.)


Related tools
=============

deML
----

Code: https://github.com/grenaud/deml
Paper: https://doi.org/10.1093/bioinformatics/btu719

Like qaf-demux, deML uses likelihoods to assign a read to the most likely
index. However, it does not seem to directly process fastq data with
inline barcode starting at an arbitrary position. The barcodes need to be
extracted beforehand and provided as separate fastq files.

The likelihood calculation slightly differs with the approach used in
qaf-demux: In qaf-demux, the probability of a real match when a mismatch
is observed is one third of the predicted error rate at the considered
base, reasoning that if the base is not read correctly, it can be either
one of the three other possible bases. In deML, this probability is the
predicted error rate, without dividing by three. Commentaries by
specialists of probabilities would be welcome in this regard.


QIIME 2
-------

Code: https://github.com/qiime2/qiime2
Paper: https://peerj.com/preprints/27295/ (temporary citation)

QIIME 2 seems to have plugins to demultiplex data with inline barcode,
but the barcode is then expected to be at the beginning of the read. For
instance:
https://docs.qiime2.org/2018.11/plugins/available/cutadapt/demux-single/
and
https://forum.qiime2.org/t/demultiplexing-and-trimming-adapters-from-reads-with-q2-cutadapt/2313/19

Not sure this uses base qualities either.
